from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import select, delete, Column, null
from models.itemf import ItemF, ItemFsModel

class ItemFsRepo():
    def get_all(self, session: Session):
        return session.execute(select(ItemF).order_by(Column('id'))).scalars().all()

    def get_by_id(self, id: int, session: Session):
        return session.execute(select(ItemF).where(Column('id') == id)).scalar()

    #def get_by_name(self, name: str, session: Session):
    #    return session.execute(select(ItemF).where(ItemF.nombre_cliente.ilike(f'%{name}%'))).scalars().all()
    
    def set_itemf(self, data: ItemFsModel, session: Session):
        instance = ItemF(nro_factura = data.nro_factura, serie_mueble = data.serie_mueble)
        session.add(instance)
        session.commit()
        return instance

    """ def delete_factura(self, id: int, session: Session):
        instance = session.get(Factura, id)
        if instance is None:
            return False
        try:
            session.delete(instance)
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar el Factura")
        return True """

    """ def update_factura(self, id, data: FacturasModel, session: Session):
        instance = session.get(Factura, id)
        if instance is None:
            return False

        try:
            if data.categoria != "string" and data.categoria != "":
                instance.categoria = data.categoria
            if data.subcategoria != "string" and data.subcategoria != "":
                instance.subcategoria = data.subcategoria
            if data.id != '':
                instance.id = data.id
            if data.id_fabricante !='':
                instance.id_fabricante = data.id_fabricante
            if data.precio > 0:
                instance.precio = data.precio
            if data.fecha_fabricacion != '':
                instance.fecha_fabricacion = data.fecha_fabricacion
            
            
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar este Factura")
        return instance """