from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import and_, select, delete, Column, null, update, func
from models.mueble import Mueble, MueblesModel
import pdb
from datetime import date


#consulta = select(Mueble).where(Column('nro_serie') == id) #esto hace que podamos buscar por otra cosa, tipo consulta de bdd
#instance = session.execute(consulta).scalar()
#query = query.join(Song, Song.id == UserSong.song_id) ejemplo de unir tablas

class MueblesRepo():
    def get_all(self, session: Session):
        return session.execute(select(Mueble).order_by(Column('id_categoria'))).scalars().all()

    def get_by_serial(self, nro_serie: str, session: Session):
        return session.execute(select(Mueble).where(Column('nro_serie') == nro_serie)).scalar()
    
    def get_by_fabricantes(self, id_fabricante: int, session: Session):
        return session.execute(select(Mueble).order_by('id_categoria').where(Column('id_fabricante') == id_fabricante)).scalars().all()
    
    def get_by_categoria(self, id_categoria: int, session: Session):
        return session.execute(select(Mueble).order_by('id_fabricante').where(Column('id_categoria') == id_categoria)).scalars().all()

    def get_disponibles(self, session: Session):
        return session.execute(select(Mueble).order_by('id_categoria').where(Column('disponible') == 1)).scalars().all()
    
    def set_mueble(self, data: MueblesModel, session: Session):

        if (data.fecha_fabricacion > date.today() or data.precio <=0 or data.nro_serie=="string" or data.id_categoria == 0 or data.id_fabricante == 0):
            raise HTTPException(status_code=400,detail="Datos ilogicos")
        else:
            instance = Mueble(nro_serie = data.nro_serie, id_categoria = data.id_categoria, fecha_fabricacion = data.fecha_fabricacion, precio = data.precio, id_fabricante = data.id_fabricante)
            session.add(instance)
            session.commit()
        return instance

    def delete_mueble(self, nro_serie, session: Session):
        instance = session.get(Mueble, nro_serie)
        if instance is None:
            return False
        try:
            if (instance.disponible == 1):
                session.delete(instance)
                session.commit()
            else:
              raise HTTPException(status_code=400, detail="No se puede eliminar el Mueble")  
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar el Mueble")
        return True

    def update_mueble(self, nro_serie, data: MueblesModel, session: Session):
        instance = session.get(Mueble, nro_serie)
        if instance is None:
            return False

        try:
            if (data.fecha_fabricacion > date.today() or data.precio <=0 or data.nro_serie == "string" or data.id_categoria == 0 or data.id_fabricante == 0):
                raise HTTPException(status_code=400,detail="Datos ilogicos")
            else:
                instance.precio = data.precio
                instance.fecha_fabricacion = data.fecha_fabricacion
                instance.id_fabricante = data.id_fabricante
                instance.id_categoria = data.id_categoria
                session.commit()  
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar este Mueble")
        return instance

    def precio_mueble(self, nro_serie, nprecio, session: Session):
        instance = session.get(Mueble,nro_serie)
        if instance is None:
            return False
        else:
            if (nprecio <=0):
                raise HTTPException(status_code=400,detail="Monto invalido")
            else:
                instance.precio = nprecio
                session.commit()
                
        return instance
    
    def sell_mueble(self, nro_serie, session: Session):
        instance = session.get(Mueble, nro_serie)
        
        #pdb.set_trace()
        if instance is None:
            return False
        try: 
            if instance.disponible !=0:
                #pdb.set_trace()
                instance.disponible = 0
                session.commit()
        except:
            raise HTTPException(status_code=400, detail="El mueble ya esta vendido")
        return instance

    def back_mueble(self, nro_serie, session: Session):
        instance = session.get(Mueble,nro_serie)
        
        if instance is None:
            return False
        try:
            if instance.disponible !=1:
                instance.disponible = 1
                session.commit()
        except:
             raise HTTPException(status_code=400, detail="El mueble no esta vendido")
        return instance
    
    def contar_stock(self, id_categoria, session: Session):

        query = session.query(Mueble)
        query = query.filter(
                    and_(
                    Mueble.id_categoria == id_categoria, 
                    Mueble.disponible == 1
                    )
            )
        query = query.with_entities(func.count())
        stock = query.scalar()
        return stock
