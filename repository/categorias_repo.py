from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import select, delete, Column, null
from models.categoria import Categoria, CategoriasModel

class CategoriasRepo():
    def get_all(self, session: Session):
        return session.execute(select(Categoria).order_by(Column('id'))).scalars().all()

    def get_by_id(self, id: int, session: Session):
        return session.execute(select(Categoria).where(Column('id') == id)).scalar()

    def get_by_name(self, name: str, session: Session):
        return session.execute(select(Categoria).where(Categoria.categoria.ilike(f'%{name}%'))).scalars().all()
    
    def set_categoria(self, data: CategoriasModel, session: Session):
        if (data.categoria =="string" or data.subcategoria == "string" or data.categoria == " " or data.subcategoria == " "):
            raise HTTPException(status_code=400,detail="Datos ilogicos")
        else:
            instance = Categoria(categoria = data.categoria, subcategoria = data.subcategoria)
            session.add(instance)
            session.commit()
        return instance

    def delete_categoria(self, id: int, session: Session):
        instance = session.get(Categoria, id)
        if instance is None:
            return False
        try:
            session.delete(instance)
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar esta categoria por vinculaciones")
        return True

    def update_categoria(self, id, data: CategoriasModel, session: Session):
        instance = session.get(Categoria, id)
        if instance is None:
            return False
        try:
            if (data.categoria =="string" or data.subcategoria == "string" or data.categoria == " " or data.subcategoria == " "):
                raise HTTPException(status_code=400,detail="Datos ilogicos")
            else:
                instance.categoria = data.categoria
                instance.subcategoria = data.subcategoria            
                session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar esta categoria")
        return instance

    