from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import select, delete, Column, null
from models.pedido import Pedido, PedidosModel
import pdb
from datetime import date

class PedidosRepo():
    def get_all(self, session: Session):
        return session.execute(select(Pedido).order_by(Column('nrop'))).scalars().all()

    def get_all_activos(self, session: Session):
        return session.execute(select(Pedido).order_by(Column('fecha_arribo')).where(Column('estado') == 0)).scalars().all()

    def get_by_nrop(self, nrop:int, session: Session):
        return session.execute(select(Pedido).where(Column('nrop') == nrop)).scalar()

    def get_by_fecha(self, fecha: str, session: Session):
        return session.execute(select(Pedido).where(Column('fecha_arribo') == fecha)).scalars().all()
    
    def get_by_estado(self, nrop: int, session: Session):
        return session.execute(select(Pedido).where(Column('estado') == nrop)).scalars().all()
    
    def set_pedido(self, data: PedidosModel, session: Session):
        if (data.fecha_arribo < date.today() or data.cantidad <=0 or data.fecha > data.fecha_arribo or data.id_categoria == 0 or data.id_fabricante == 0):
            raise HTTPException(status_code=400,detail="Datos ilogicos")
        else:
            instance = Pedido(fecha = data.fecha, fecha_arribo = data.fecha_arribo, cantidad = data.cantidad, id_categoria = data.id_categoria, id_fabricante = data.id_fabricante)
            session.add(instance)
            session.commit()
        return instance

    def delete_pedido(self, nrop: int, session: Session):
        instance = session.get(Pedido, nrop)
        if instance is None:
            return False
        try:
            session.delete(instance)
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar el Pedido")
        return True

    def update_pedido(self, nrop, data: PedidosModel, session: Session):
        instance = session.get(Pedido, nrop)
        if instance is None:
            return False

        try:
            if (data.fecha_arribo < date.today() or data.cantidad <=0 or data.fecha > data.fecha_arribo or data.id_categoria == 0 or data.id_fabricante == 0):
                raise HTTPException(status_code=400,detail="Datos ilogicos")
            else:
                instance.fecha_arribo = data.fecha_arribo
                instance.cantidad = data.cantidad
                instance.fecha = data.fecha

                session.commit()               
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar este Pedido")
        return instance

    def llego_pedido(self, nrop, session: Session):
        instance = session.get(Pedido, nrop)
        
        #pdb.set_trace()
        if instance is None:
            return False
        try: 
            if instance.estado != 1:
                #pdb.set_trace()
                instance.estado = 1
                session.commit()
        except:
            raise HTTPException(status_code=400, detail="El pedido ya llego")
        return instance