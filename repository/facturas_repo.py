from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import select, delete, Column, null
from models.factura import Factura, FacturasModel
from repository.muebles_repo import MueblesRepo
from models.mueble import Mueble, MueblesModel, MueblesAPI

auxMueble = MueblesRepo()
class FacturasRepo():

    

    def get_all(self, session: Session):
        return session.execute(select(Factura).order_by(Column('nro'))).scalars().all()

    def get_by_nro(self, nro: int, session: Session):
        return session.execute(select(Factura).where(Column('nro') == nro)).scalar()

    def get_by_name(self, name: str, session: Session):
        return session.execute(select(Factura).where(Column('nombre_cliente') == name)).scalars().all()
    
    def set_factura(self, data: FacturasModel, session: Session):
        if (data.dni <= 0 or data.nombre_cliente == "string" or data.nombre_cliente == "" or data.nombre_cliente == " " or data.serie_mueble == "string" or data.serie_mueble== "" or data.serie_mueble== " "):
            raise HTTPException(status_code=400,detail="Datos ilogicos")
        else:
            try:
                aux = auxMueble.get_by_serial(data.serie_mueble,session)
                if(aux.disponible == 1):
                    instance = Factura(dni = data.dni,nombre_cliente = data.nombre_cliente, serie_mueble = data.serie_mueble)
                    session.add(instance)
                    session.commit()
                    auxMueble.sell_mueble(data.serie_mueble, session)
                else:
                    raise HTTPException(status_code=400,detail="Mueble ya esta vendido")
            except:
               raise HTTPException(status_code=404,detail="Mueble no existe") 
        return instance

"""     def update_factura(self, id, data: FacturasModel, session: Session):
        instance = session.get(Factura, id)
        if instance is None:
            return False

        try:
            if data.total > 0 and instance.total < data.total:
                instance.total = data.total
            
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar esta Factura")
        return instance """


""" def delete_factura(self, id: int, session: Session):
        instance = session.get(Factura, id)
        if instance is None:
            return False
        try:
            session.delete(instance)
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar el Factura")
        return True """