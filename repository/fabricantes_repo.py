from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
from sqlalchemy import null, select, delete, Column
from models.fabricante import Fabricante, FabricantesModel

class FabricantesRepo():
    def get_all(self, session: Session):
        return session.execute(select(Fabricante).order_by(Column('id'))).scalars().all()

    def get_by_id(self, id: int, session: Session):
        return session.execute(select(Fabricante).where(Column('id') == id)).scalar()

    def get_by_name(self, name: str, session: Session):
        return session.execute(select(Fabricante).where(Fabricante.nombre.ilike(f'%{name}%'))).scalars().all()
    
    def set_fabricante(self, data: FabricantesModel, session: Session):
        if (data.nombre =="string" or data.direccion == "string" or data.nombre == " " or data.direccion == " " or data.nombre == "" or data.direccion == "" or data.telefono == 0 or data.contacto =="string" or data.observaciones == "string" or data.contacto == " " or data.observaciones == " " or data.contacto == "" or data.observaciones == ""):
            raise HTTPException(status_code=400,detail="Datos ilogicos")
        else:
            instance = Fabricante(nombre = data.nombre, direccion = data.direccion, telefono = data.telefono, contacto = data.contacto, observaciones = data.observaciones)
            session.add(instance)
            session.commit()
        return instance

    def delete_fabricante(self, id: int, session: Session):
        instance = session.get(Fabricante, id)
        if instance is None:
            return False
        try:
            session.delete(instance)
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede eliminar el Fabricante porque tiene vinculaciones")
        return True

    def update_fabricante(self, id, data: FabricantesModel, session: Session):
        instance = session.get(Fabricante, id)
        if instance is None:
            return False

        try:
            if data.nombre != "string" and data.nombre != "" and data.nombre != " ":
                instance.nombre = data.nombre
            if data.direccion != "string" and data.direccion != "" and data.direccion != " ":
                instance.direccion = data.direccion
            if data.telefono != 0:
                instance.telefono = data.telefono
            if data.contacto != "string" and data.contacto != "" and data.contacto != " ":
                instance.contacto = data.contacto
            if data.observaciones != "string" and data.observaciones != "" and data.observaciones != " ":
                instance.observaciones = data.observaciones
            
            session.commit()
        except:
            raise HTTPException(status_code=400, detail="No se puede actualizar este Fabricante")
        return instance