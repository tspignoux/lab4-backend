from datetime import date
from email.mime import base
from enum import unique
from tokenize import Double
from sqlalchemy.sql.schema import ForeignKey
from db import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Date, Float, Integer, String, false
from pydantic import BaseModel

class Mueble(Base):
     __tablename__ = 'muebles'
     nro_serie = Column(String(50), nullable=False , primary_key = True, unique=True)
     id_categoria = Column(ForeignKey('categorias.id'),nullable=False)
     categoria = relationship('Categoria', lazy='joined')
     fecha_fabricacion = Column(Date)
     precio = Column(Float, nullable = False)
     disponible = Column(Integer, nullable=False, default=1)
     id_fabricante = Column(ForeignKey('fabricantes.id'), nullable = False)
     fabricante = relationship('Fabricante', lazy='joined')

class MueblesModel(BaseModel):
     nro_serie: str
     id_categoria: int
     fecha_fabricacion: date
     precio: float
     id_fabricante: int
     
     class Config:
          orm_mode = True

class MueblesAPI(MueblesModel):
     nro_serie: str

     