from datetime import date
from email.mime import base
from email.policy import default
from pickle import BINFLOAT
from tokenize import Double
from sqlalchemy.sql.schema import ForeignKey
from db import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Date, Float, Integer, String, false, BigInteger
from pydantic import BaseModel

class Factura(Base):
    __tablename__ = 'facturas'
    nro = Column(Integer, primary_key = True, nullable=False)
    dni = Column(BigInteger, nullable=False)
    nombre_cliente = Column(String(50), nullable = False)
    serie_mueble = Column(ForeignKey('muebles.nro_serie'), nullable=False, unique=True)
    mueble = relationship('Mueble', lazy='joined')
    

class FacturasModel(BaseModel):
    dni: int
    nombre_cliente: str
    serie_mueble: str
    
    class Config:
        orm_mode = True

class FacturasAPI(FacturasModel):
    nro: int