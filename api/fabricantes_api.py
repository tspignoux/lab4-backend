from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.fabricantes_repo import FabricantesRepo
from models.fabricante import FabricantesModel, FabricantesAPI

fabricantes_router = APIRouter(prefix='/fabricantes', tags=['Fabricantes'])
repo = FabricantesRepo()

@fabricantes_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session) 

@fabricantes_router.get('/search/{name}')
def get_by_name(name, session: Session = Depends(get_session)):
    return repo.get_by_name(name, session)

@fabricantes_router.get('/{id}')
def get_by_id(id, session: Session = Depends(get_session)):
    fabricante = repo.get_by_id(id, session)
    if fabricante:
        return fabricante
    else:
        raise HTTPException(status_code=404, detail="Fabricante no existente")

@fabricantes_router.post('/', response_model=FabricantesAPI)
def set_fabricante(data: FabricantesModel, session: Session = Depends(get_session)):
    fabricante = repo.set_fabricante(data, session)
    return fabricante

@fabricantes_router.delete('/{id}')
def delete_fabricante(id, session: Session = Depends(get_session)):
    result = repo.delete_fabricante(id, session)
    if not result:
        raise HTTPException(status_code=404, detail="Fabricante no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente el fabricante")

@fabricantes_router.put('/{id}', response_model=FabricantesAPI)
def update_fabricante(id, data: FabricantesModel, session: Session = Depends(get_session)):
    fabricante = repo.update_fabricante(id, data, session)
    if not fabricante:
        raise HTTPException(status_code=404, detail="Fabricante no existente")
    else:
        return fabricante
