from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.pedidos_repo import PedidosRepo
from models.pedido import PedidosModel, PedidosAPI

pedidos_router = APIRouter(prefix='/pedidos', tags=['Pedidos'])
repo = PedidosRepo()

@pedidos_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session)

@pedidos_router.get('/activos')
def get_all_activos(session: Session = Depends(get_session)):
    return repo.get_all_activos(session) 

@pedidos_router.get('/search/{fecha}')
def get_by_fecha(fecha, session: Session = Depends(get_session)):
    return repo.get_by_fecha(fecha, session)

@pedidos_router.get('/{nrop}')
def get_by_nrop(nrop, session: Session = Depends(get_session)):
    pedido = repo.get_by_nrop(nrop, session)
    if pedido:
        return pedido
    else:
        raise HTTPException(status_code=404, detail="pedido no existente")

@pedidos_router.post('/', response_model=PedidosAPI)
def set_pedido(data: PedidosModel, session: Session = Depends(get_session)):
    pedido = repo.set_pedido(data, session)
    return pedido

@pedidos_router.delete('/{nrop}')
def delete_pedido(nrop, session: Session = Depends(get_session)):
    result = repo.delete_pedido(nrop, session)
    if not result:
        raise HTTPException(status_code=404, detail="pedido no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente el pedido")

@pedidos_router.put('/{nrop}', response_model=PedidosAPI)
def update_pedido(nrop, data: PedidosModel, session: Session = Depends(get_session)):
    pedido = repo.update_pedido(nrop, data, session)
    if not pedido:
        raise HTTPException(status_code=404, detail="pedido no existente")
    else:
        return pedido

@pedidos_router.patch('/{nrop}', response_model=PedidosAPI)
def llego_pedido(nrop:int, session: Session = Depends(get_session)):
    pedido = repo.llego_pedido(nrop, session)
    if not pedido:
        raise HTTPException(status_code=404, detail="Pedido no existente")
    else:
        return pedido