from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.facturas_repo import FacturasRepo
from models.factura import FacturasModel, FacturasAPI

facturas_router = APIRouter(prefix='/facturas', tags=['Facturas'])
repo = FacturasRepo()

@facturas_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session) 

@facturas_router.get('/search/{name}')
def get_by_name(name, session: Session = Depends(get_session)):
    return repo.get_by_name(name, session)

@facturas_router.get('/{nro}')
def get_by_nro(nro, session: Session = Depends(get_session)):
    factura = repo.get_by_nro(nro, session)
    if factura:
        return factura
    else:
        raise HTTPException(status_code=404, detail="factura no existente")

@facturas_router.post('/', response_model=FacturasAPI)
def set_factura(data: FacturasModel, session: Session = Depends(get_session)):
    factura = repo.set_factura(data, session)
    return factura

""" @facturas_router.delete('/{id}')
def delete_factura(id, session: Session = Depends(get_session)):
    result = repo.delete_factura(id, session)
    if not result:
        raise HTTPException(status_code=404, detail="Factura no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente el Factura") """

""" @facturas_router.put('/{id}', response_model=FacturasAPI)
def update_factura(id, data: FacturasModel, session: Session = Depends(get_session)):
    factura = repo.update_factura(id, data, session)
    if not factura:
        raise HTTPException(status_code=404, detail="factura no existente")
    else:
        return factura """
