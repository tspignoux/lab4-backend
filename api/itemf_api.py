from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.itemf_repo import ItemFsRepo
from models.itemf import ItemFsModel, ItemFsAPI

itemfs_router = APIRouter(prefix='/itemfs', tags=['ItemFs'])
repo = ItemFsRepo()

@itemfs_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session) 

@itemfs_router.get('/search/{name}')
def get_by_name(name, session: Session = Depends(get_session)):
    return repo.get_by_name(name, session)

@itemfs_router.get('/{id}')
def get_by_id(id, session: Session = Depends(get_session)):
    itemf = repo.get_by_id(id, session)
    if itemf:
        return itemf
    else:
        raise HTTPException(status_code=404, detail="itemf no existente")

@itemfs_router.post('/', response_model=ItemFsAPI)
def set_itemf(data: ItemFsModel, session: Session = Depends(get_session)):
    itemf = repo.set_itemf(data, session)
    return itemf

""" @facturas_router.delete('/{id}')
def delete_factura(id, session: Session = Depends(get_session)):
    result = repo.delete_factura(id, session)
    if not result:
        raise HTTPException(status_code=404, detail="Factura no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente el Factura") """

""" @facturas_router.put('/{id}', response_model=FacturasAPI)
def update_factura(id, data: FacturasModel, session: Session = Depends(get_session)):
    factura = repo.update_factura(id, data, session)
    if not factura:
        raise HTTPException(status_code=404, detail="factura no existente")
    else:
        return factura """
